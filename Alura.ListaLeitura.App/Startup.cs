﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;


namespace Alura.ListaLeitura.App
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
        }
        public void Configure(IApplicationBuilder app)
        {
            app.UseDeveloperExceptionPage();
            app.UseMvcWithDefaultRoute();
            /*
            var builder = new RouteBuilder(app);
            builder.MapRoute("{classe}/{metodo}", RoteamentoPadrao.TratamentoPadrao);
            builder.MapRoute("Livros/ParaLer", LivrosController.ParaLer);
            builder.MapRoute("Livros/Lendo", LivrosController.Lendo);
            builder.MapRoute("Livros/Lidos", LivrosController.Lidos);
            builder.MapRoute("Livros/Adicionar/{titulo}/{autor}", LeituraLogica.Adicionar);
            builder.MapRoute("Livros/EstouLendo/{id}", LivrosController.EstouLendo);
            builder.MapRoute("Livros/Formulario", LeituraLogica.Formulario);
            builder.MapRoute("Livros/Adicionar", LeituraLogica.Adicionar);
            app.UseRouter(builder.Build());
            app.Run(Roteamento);*/
        }

    }
}