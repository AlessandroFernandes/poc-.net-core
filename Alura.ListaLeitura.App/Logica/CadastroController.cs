﻿using Alura.ListaLeitura.App.Negocio;
using Alura.ListaLeitura.App.Repositorio;
using Microsoft.AspNetCore.Mvc;

namespace Alura.ListaLeitura.App.Logica
{
    public class CadastroController
    {
        public IActionResult Formulario()
        {
            var html = new ViewResult { ViewName = "Formulario"};
            return html;
        }
        public string Adicionar(Livro livro)
        {
            var repo = new LivroRepositorioCSV();
            repo.Incluir(livro);
            return "Livro adicionado com sucesso";
        }

    }
}
