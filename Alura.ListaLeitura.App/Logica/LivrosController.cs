﻿using Alura.ListaLeitura.App.Negocio;
using Alura.ListaLeitura.App.Repositorio;
using Alura.ListaLeitura.App.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alura.ListaLeitura.App.Logica
{
    public class LivrosController : Controller

    {
        public IEnumerable<Livro> Livros { get; set; }
        public IActionResult ParaLer()
        {
            var _repo = new LivroRepositorioCSV();
            ViewBag.Livros = _repo.ParaLer.Livros;
            return View("ParaLer");
        }
        public string EstouLendo(int id)
        {
            var repo = new LivroRepositorioCSV();
            var livro = repo.Todos.First(lv => lv.Id == id);
            return livro.Detalhes();
        }
        public IActionResult Lendo()
        {
            var _repositorio = new LivroRepositorioCSV();
            ViewBag.Livros = _repositorio.Lendo.Livros;
            return View("ParaLer");
        }

        public IActionResult Lidos()
        {
            var _repositorio = new LivroRepositorioCSV();
            ViewBag.Livros = _repositorio.Lidos.Livros;
            return View("ParaLer");
        }
    }
}
