﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Alura.ListaLeitura.App.Utils
{
    public class LeituraHtml
    {
        public static string retornarFormulario(string pagina)
        {
            var enderecoCompleto = $"Html/{pagina}.html";
            using (var arquivo = File.OpenText(enderecoCompleto))
            {
                return arquivo.ReadToEnd();
            }
        }

    }
}
